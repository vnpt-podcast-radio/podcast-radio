const express = require("express");
const UserController = require("../controllers/UserController");
const SingerController = require("../controllers/SingerController");
const AuthMiddleware = require("../../middlewares/authMiddlewares");
const PermissionMiddleware = require("../../middlewares/permissionMiddleware");
const SongController = require("../controllers/SongController");
const RoomController = require("../controllers/RoomController");
const router = express.Router();

const upload = require("../../middlewares/multerFile");
const RoleController = require("../controllers/RoleController");

//USER ENDPOINT
router.post("/user/login", UserController.Login);
router.post("/user/register", UserController.createNewUser);
router.get("/users", AuthMiddleware.Authenticate, UserController.getAllUsers);
router.get(
    "/user/current",
    AuthMiddleware.Authenticate,
    UserController.getCurrentUser
);
router.get("/user/:id", AuthMiddleware.Authenticate, UserController.getUser);
router.post(
    "/user/refreshToken",
    AuthMiddleware.Authenticate,
    UserController.refreshToken
);
router.post(
    "/user/upload-avatar",
    AuthMiddleware.Authenticate,
    upload.single("file"),
    UserController.uploadAvatar
);

//SINGER ENDPOINT
router.post(
    "/singer/create",
    AuthMiddleware.Authenticate,
    PermissionMiddleware.checkPermission("Admin"),
    SingerController.createNewSinger
);
router.get("/singers", SingerController.getAllSingers);
router.get("/singer/:id", SingerController.getCurrentSinger);
router.put(
    "/singer/rename/:id",
    AuthMiddleware.Authenticate,
    PermissionMiddleware.checkPermission("Admin"),
    SingerController.renameSinger
);

//SONG ENDPOINT
router.post(
    "/song/create",
    AuthMiddleware.Authenticate,
    PermissionMiddleware.checkPermission("Admin"),
    upload.single("file"),
    SongController.createNewSong
);
router.get("/songs", SongController.getAllSongs);
router.delete("/song/:id", SongController.deteleSongById);

//ROOM ENDPOINT
router.post(
    "/room/create",
    AuthMiddleware.Authenticate,
    RoomController.createRoom
);

router.get("/room/:id", RoomController.getRoomById);
router.get("/rooms", RoomController.getRooms);
router.delete(
    "/room/:id",
    AuthMiddleware.Authenticate,
    RoomController.deleteRoomById
);

//ROLE
router.post(
    "/role/create",
    AuthMiddleware.Authenticate,
    PermissionMiddleware.checkPermission("Admin"),
    RoleController.createNewRole
);
router.get("/roles", RoleController.getAllRoles);

module.exports = router;
