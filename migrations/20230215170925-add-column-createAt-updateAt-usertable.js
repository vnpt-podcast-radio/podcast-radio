'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn("Users", "createdAt", {
      type: Sequelize.DATE
    });
    await queryInterface.addColumn("Users", "updatedAt", {
      type: Sequelize.DATE
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.addColumn("Users", "createdAt");
    await queryInterface.addColumn("Users", "updatedAt");
  }
};
