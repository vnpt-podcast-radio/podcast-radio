const sequelize = require("../database/database");
const { DataTypes } = require("sequelize");
const User = require("./User");

const Room = sequelize.define("Room", {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    describe: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    userId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
            model: "Users",
            key: "id",
        },
    },
    status: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
    },
});
User.hasMany(Room, { foreignKey: "userId", as: "rooms" });
Room.belongsTo(User, { foreignKey: "userId", as: "user" });
module.exports = Room;
