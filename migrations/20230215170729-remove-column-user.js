'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.removeColumn("Users", "upadateAt");
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.addColumn("Users", "upadateAt", {
      type: Sequelize.DATE
    });
  }
};
