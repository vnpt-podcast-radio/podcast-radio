const uuid = require("uuid");
const cloudinary = require("cloudinary").v2;

cloudinary.config({
    cloud_name: "dfvskv6og",
    api_key: "941598865492711",
    api_secret: "w1fyYguEvn4xiaZE5roE1tNq-ck",
});

class MediaService {
    static async uploadAudio(req) {
        const buffer = req.file.buffer;
        const promise = new Promise((resolve, reject) => {
            const id = uuid.v4();
            cloudinary.uploader
                .upload_stream(
                    {
                        resource_type: "auto",
                        public_id: `musics/${id}`,
                    },
                    (error, result) => {
                        if (error) {
                            reject(error);
                        } else {
                            resolve(result);
                        }
                    }
                )
                .end(buffer);
        });
        return promise;
    }

    static async uploadAvatar(req) {
        const buffer = req.file.buffer;
        const promise = new Promise((resolve, reject) => {
            const id = uuid.v4();
            cloudinary.uploader
                .upload_stream(
                    {
                        resource_type: "auto",
                        public_id: `avatars/${id}`,
                    },
                    (error, result) => {
                        if (error) {
                            reject(error);
                        } else {
                            resolve(result);
                        }
                    }
                )
                .end(buffer);
        });
        return promise;
    }

    static async deleteAudio(publicId) {
        try {
            const result = await cloudinary.uploader.destroy(publicId);
            if (result.result === "ok") {
                return result;
            }
            return null;
        } catch (error) {
            console.log("Lỗi khi xóa audio: ", error);
            return null;
        }
    }
}

module.exports = MediaService;
