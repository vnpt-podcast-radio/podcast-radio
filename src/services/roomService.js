const { where } = require("sequelize");
const Room = require("../models/Room");

class RoomService {
    static async newRoom(id, name, describe, userId) {
        try {
            const room = await Room.create({ id, name, describe, userId });
            console.log(room);
            return room;
        } catch (e) {
            throw e;
        }
    }

    static async deleteRoom(id, userId) {
        const room = Room.findOne({
            where: {
                id: id,
                userId: userId,
            },
        })
            .then((room) => {
                return room.destroy();
            })
            .catch((error) => {
                return null;
            });
        return room;
    }

    static async stopRoom(id) {
        const room = Room.findOne({ where: { id: id } })
            .then((room) => {
                console.log("Room Stopped");
                return room.update({ status: false });
            })
            .catch((error) => {
                return error;
            });
        return room;
    }
}

module.exports = RoomService;
