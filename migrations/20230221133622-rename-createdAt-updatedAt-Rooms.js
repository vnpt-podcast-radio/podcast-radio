'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.renameColumn('Rooms', 'createAt', 'createdAt');
    await queryInterface.renameColumn('Rooms', 'updateAt', 'updatedAt');
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.renameColumn('Rooms', 'createdAt', 'createAt');
    await queryInterface.renameColumn('Rooms', 'updatedAt', 'updateAt');
  }
};
