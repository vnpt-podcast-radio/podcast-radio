const User = require("../models/User");
const jwt = require("jsonwebtoken");

class JWTService {
    static GenerateToken(user) {
        const email = user.email;
        const refreshToken = jwt.sign({ email }, process.env.SECRET_KEY, {
            expiresIn: "1h",
        });
        const token = jwt.sign({ user }, process.env.SECRET_KEY, {
            expiresIn: "1h",
        });
        return { token, refreshToken };
    }

    static VerifyToken(token) {
        try {
            const decoded = jwt.verify(token, process.env.SECRET_KEY);
            return decoded;
        } catch {
            return null;
        }
    }

    static VerifyRefreshToken(refreshToken) {
        try {
            const decoded = jwt.verify(refreshToken, process.env.SECRET_KEY);
            return decoded.email;
        } catch {
            return null;
        }
    }
}

module.exports = JWTService;
