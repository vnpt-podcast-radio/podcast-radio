'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('Rooms', 'createAt', {
      type: Sequelize.DATE,
    });
    await queryInterface.addColumn('Rooms', 'updateAt', {
      type: Sequelize.DATE,
    });
    await queryInterface.renameColumn('Rooms', 'desribe', 'describe');
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn('Rooms', 'createAt');
    await queryInterface.removeColumn('Rooms', 'updateAt');
    await queryInterface.renameColumn('Rooms', 'describe', 'desribe');
  }
};
