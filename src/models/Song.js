const { DataTypes } = require("sequelize");
const { Sequelize } = require("sequelize");
const sequelize = require("../database/database");
const Singer = require("./Singer");

const Song = sequelize.define("Song", {
    id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
    },
    name: {
        allowNull: false,
        type: DataTypes.STRING,
    },
    url_song: {
        allowNull: false,
        type: DataTypes.STRING,
    },
    public_id: {
        type: DataTypes.STRING,
    },
});

Song.associations = () => {
    Song.belongsToMany(Singer, {
        through: "SingerSongs",
        foreignKey: "singerId",
    });
};

module.exports = Song;
