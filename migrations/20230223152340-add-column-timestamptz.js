'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn('Rating', 'createdAt', {
      type: Sequelize.DATE
    });
    await queryInterface.addColumn('Rating', 'updatedAt', {
      type: Sequelize.DATE
    });
    await queryInterface.addColumn('Gift', 'createdAt', {
      type: Sequelize.DATE
    });
    await queryInterface.addColumn('Gift', 'updatedAt', {
      type: Sequelize.DATE
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn('Rating', 'createdAt');
    await queryInterface.removeColumn('Gift', 'createdAt');
    await queryInterface.removeColumn('Rating', 'updatedAt');
    await queryInterface.removeColumn('Gift', 'updatedAt');
  }
};
