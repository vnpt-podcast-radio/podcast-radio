const bcrypt = require("bcryptjs");

class PasswordService {
    async hashPassword(password) {
        const hashedPassword = bcrypt.hash(
            password,
            parseInt(process.env.SALT_ROUNDS)
        );
        return hashedPassword;
    }
}

module.exports = PasswordService;
