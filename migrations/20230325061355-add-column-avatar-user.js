"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addColumn("Users", "avatar", {
            type: Sequelize.STRING,
            allowNull: true,
        });
        await queryInterface.addColumn("Users", "avatar_public_id", {
            type: Sequelize.STRING,
            allowNull: true,
        });
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeColumn("Users", "avatar");
        await queryInterface.removeColumn("Users", "avatar_public_id");
    },
};
