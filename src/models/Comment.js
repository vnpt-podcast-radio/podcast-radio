const { DataTypes } = require("sequelize");
const { Sequelize } = require("sequelize");
const sequelize = require("../database/database");
const User = require("./User");
const Room = require("./Room");

const Comment = sequelize.define("Comment", {
    id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
    },
    content: {
        allowNull: false,
        type: DataTypes.STRING,
    },
});

Room.hasMany(Comment, { foreignKey: "roomId", as: "comments" });
Comment.belongsTo(Room, { foreignKey: "roomId", as: "room" });
User.hasMany(Comment, { foreignKey: "userId", as: "comments" });
Comment.belongsTo(User, { foreignKey: "userId", as: "users" });

module.exports = Comment;
