const Role = require("../models/Role");
const RoleService = require("../services/roleService");
const UserService = require("../services/userService");

class RoleController {
    static async createNewRole(req, res, next) {
        try {
            const { name } = req.body;
            if (!name) {
                return res.status(400).json({ error: "Please provide name" });
            }

            console.log(req.body);
            const newRole = await RoleService.NewRole(name);
            if (newRole) {
                return res
                    .status(201)
                    .json({ message: "Create new Role successlly!" });
            }
            res.status(400).json({ error: "Create new Role failed!" });
        } catch (err) {
            next(err);
        }
    }

    static async getAllRoles(req, res) {
        const roles = await RoleService.getAllRoles();
        return res.status(200).json({
            status: 200,
            data: roles,
        });
    }
}

module.exports = RoleController;
