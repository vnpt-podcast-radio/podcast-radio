# PODCAST RADIO API

## Bắt Đầu / Start

### Hướng dẫn cài đặt / Installation Instructions

#### 1. Clone:

```
    git clone <repository>
```

#### 2. Install Package:

-   Command:

```
    npm install
```

#### 3. Create `.env` from `.env.example` with the same content and level folder (contents like below):

-   SECRET_KEY="" \\ Secret Key
-   DB_USER="" \\ Database username
-   DB_PASSWORD="" \\ Database password
-   DB_NAME="" \\ Database name
-   DB_HOST="" \\ Host Exam: localhost || 127.0.0.1
-   DB_PORT= \\ Port Exam: 3999
-   API_PORT= \\ Port running Api
-   SALT_ROUNDS= \\ Cai ni quen roi
-   DB_DIALECT="" \\ Dialect Database Exam: postgres || mysql ...

#### 4. Create the first database:

-   Command:

```
    npm run cre-db
```

#### 5. Update database:

-   Update:

```
    npx sequelize-cli db:migrate --config=config/config.js
```

or

```
    sequelize db:migrate --config=config.js \\install global sequelize-cli
```

#### 6. Run Server:

-   Run with Developer:

```
   npm start
```
