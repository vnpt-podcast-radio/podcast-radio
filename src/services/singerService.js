const Singer = require("../models/Singer");
const Song = require("../models/Song");
const uuid = require("uuid");

class SingerService {
  //CREATE SINGER
  static async createNewSinger(name) {
    const id = uuid.v4();
    try {
      const singer = Singer.create({ id, name });
      console.log("Successfully!");
      return singer;
    } catch (err) {
      throw err;
    }
  }

  //CHECK NAME SINGER
  static async checkSingerByName(name) {
    const singer = Singer.findOne({
      where: {
        name: name,
      },
    });
    return singer;
  }

  //CHECK ID SINGER
  static async checkSingerById(id) {
    const singer = Singer.findOne({
      where: {
        id: id,
      },
    });
    return singer;
  }

  //GET SINGER BY ID
  static async getCurrentSinger(id) {
    const singer = await Singer.findOne({
      where: {
        id: id,
      },
      include: [
        {
          model: Song,
          as: "songs",
        },
      ],
    });
    return singer;
  }

  //RENAME SINGER
  static async renameSinger(id, name) {
    try {
      const singer = await Singer.findByPk(id);
      if (!singer) {
        return null;
      }
      singer.name = name;
      const updatedSinger = await singer.save();
      return updatedSinger;
    } catch (err) {
      throw err;
    }
  }
}

module.exports = SingerService;
