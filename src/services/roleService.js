const Role = require("../models/Role");
const uuid = require("uuid");
const User = require("../models/User");

class RoleService {
  static async NewRole(name) {
    const id = uuid.v4();
    try {
      const role = Role.create({ id, name });
      console.log("Successfully!");
      return role;
    } catch (err) {
      throw err;
    }
  }

  static async getAllRoles() {
    const roles = await Role.findAll({
      include: {
        as: "users",
        model: User,
      },
    });
    return roles;
  }
}

module.exports = RoleService;
