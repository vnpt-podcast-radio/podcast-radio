const SingerService = require("../services/singerService");
const { json } = require("body-parser");
const Singer = require("../models/Singer");
const Song = require("../models/Song");
const { validate: uuidValidate } = require("uuid");

class SingerController {
    static async createNewSinger(req, res, next) {
        try {
            const { name } = req.body;
            if (!name) {
                return res.status(400).json({ error: "Please provide name" });
            }

            console.log(req.body);

            const existingSinger = await SingerService.checkSingerByName(name);
            if (existingSinger) {
                return res
                    .status(400)
                    .json({ error: "Singer is already exitsts" });
            }

            const newSinger = await SingerService.createNewSinger(name);
            if (newSinger) {
                return res
                    .status(201)
                    .json({ message: "Create new Singer successlly!" });
            }
            res.status(400).json({ error: "Create new Singer failed!" });
        } catch (err) {
            next(err);
        }
    }

    static async getAllSingers(req, res) {
        const singers = await Singer.findAll({
            include: {
                as: "songs",
                model: Song,
            },
        });
        return res.status(200).json({
            data: singers,
        });
    }

    static async getCurrentSinger(req, res) {
        const id = req.params.id;
        const isValidUUID = uuidValidate(id);
        if (!isValidUUID) {
            return res.status(400).json({ error: "Invalid Singer ID" });
        }
        const singer = await SingerService.getCurrentSinger(id);
        if (!singer) {
            return res
                .status(400)
                .json({ message: `Singer do not exist with ${id}` });
        }
        return res.status(200).json({
            status: 200,
            data: singer,
        });
    }

    static async renameSinger(req, res, next) {
        try {
            const id = req.params.id;
            const { name } = req.body;
            if (!name) {
                return res
                    .status(400)
                    .json({ error: "Please provide new name" });
            }

            const existingSingerById = await SingerService.checkSingerById(id);
            if (!existingSingerById) {
                return res.status(400).json({ error: "Singer not found" });
            }

            const existingSingerByName = await SingerService.checkSingerByName(
                name
            );
            if (existingSingerByName) {
                return res
                    .status(400)
                    .json({ error: "Singer is already exitsts" });
            }
            const updatedSinger = await SingerService.renameSinger(id, name);
            if (updatedSinger) {
                return res
                    .status(200)
                    .json({ message: "Singer name updated successfully" });
            }

            res.status(400).json({ error: "Failed to update singer name" });
        } catch (err) {
            next(err);
        }
    }
}

module.exports = SingerController;
