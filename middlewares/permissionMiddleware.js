const express = require("express");
const Role = require("../src/models/Role");
const User = require("../src/models/User");

class PermissionMiddleware {
  static checkPermission(permission) {
    return async function (req, res, next) {
      const roleId = req.user.user.roleId;
      if (!roleId) {
        return res.status(401).json({ message: "Unauthorized" });
      }

      const role = await Role.findOne({
        where: {
          id: roleId,
        },
      });

      if (!role) {
        return res.status(400).json({ message: "Role not found" });
      }
      if (role.name != permission) {
        return res.status(403).json({ message: "Insufficient permissions" });
      } else {
        next();
      }
    };
  }
}

module.exports = PermissionMiddleware;
