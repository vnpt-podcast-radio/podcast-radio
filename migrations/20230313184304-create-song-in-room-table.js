"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable("RoomSongs", {
            id: {
                type: Sequelize.UUID,
                allowNull: false,
                primaryKey: true,
                defaultValue: Sequelize.UUIDV4,
            },
            roomId: {
                type: Sequelize.UUID,
                allowNull: false,
                references: {
                    model: "Rooms",
                    key: "id",
                },
            },
            songId: {
                type: Sequelize.UUID,
                allowNull: false,
                references: {
                    model: "Songs",
                    key: "id",
                },
            },
            time_start: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            time_end: {
                type: Sequelize.DATE,
                allowNull: true,
            },
            createdAt: {
                type: Sequelize.DATE,
            },
            updatedAt: {
                type: Sequelize.DATE,
            },
        });
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable("RoomSongs");
    },
};
