'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable("SingerSongs", {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true
      },
      singerId: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: "Singers",
          },
          key: 'id'
        },
        allowNull: false,
        onDelete: 'CASCADE'
      },
      songId: {
        type: Sequelize.UUID,
        references: {
          model: {
            tableName: 'Songs',
          },
          key: 'id'
        },
        allowNull: false,
        onDelete: 'CASCADE'
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false
      }
    })
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable("SingerSongs");
  }
};
