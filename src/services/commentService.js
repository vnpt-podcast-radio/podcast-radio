const { validate: uuidValidate } = require("uuid");
const Comment = require("../models/Comment");

class CommentService {
    static SaveComment(userId, roomId, content) {
        try {
            const comment = Comment.create({ content, userId, roomId });
            return comment;
        } catch (error) {
            throw error;
        }
    }
}

module.exports = CommentService;
