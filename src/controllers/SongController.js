const SongService = require("../services/songService");
const Song = require("../models/Song");
const Singer = require("../models/Singer");
const { validate: uuidValidate } = require("uuid");

const mediaService = require("../services/mediaService");

class SongController {
    static async createNewSong(req, res, next) {
        try {
            const { name, singers } = JSON.parse(req.body.data);
            if (!name || !singers) {
                return res
                    .status(400)
                    .json({ error: "Please provide name, singer" });
            }
            await mediaService
                .uploadAudio(req)
                .then((result) => {
                    const url = result.url;
                    const public_id = result.public_id;
                    const newSong = SongService.createNewSong(
                        name,
                        url,
                        public_id,
                        singers
                    );
                    if (newSong) {
                        return res
                            .status(201)
                            .json({ message: "Created new Song!" });
                    }
                })
                .catch((error) => {
                    console.log(error.message);
                    return res
                        .status(400)
                        .json({ error: "Create new song failed" });
                });
        } catch (err) {
            next(err);
        }
    }

    static async getAllSongs(req, res) {
        const songs = await Song.findAll({
            include: {
                as: "singers",
                model: Singer,
            },
        });
        return res.status(200).json({
            data: songs,
        });
    }

    static async deteleSongById(req, res) {
        const id = req.params.id;
        const isValidUUID = uuidValidate(id);
        if (!isValidUUID) {
            return res.status(400).json({
                message: "UUID Song inValid!",
            });
        }
        const song = await SongService.deleteSong(id);
        if (!song) {
            return res.status(400).json({
                message: "NOT FOUND",
            });
        }
        return res.status(200).json({
            message: "Ok",
        });
    }
}

module.exports = SongController;
