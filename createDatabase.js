const { Sequelize } = require("sequelize");

const config = require("./config/config.js");

const sequelize = new Sequelize(
    `postgres://${config.development.username}:${config.development.password}@${config.development.host}:${config.development.port}`,
    {
        dialect: config.development.dialect,
    }
);

(async () => {
    try {
        await sequelize.query(`CREATE DATABASE ${config.development.database}`);
        console.log(
            `Database "${config.development.database}" has been created.`
        );
    } catch (err) {
        console.error(err);
    } finally {
        await sequelize.close();
    }
})();
