const UserService = require("../services/userService");
const JWTService = require("../services/jwtService");
const ValidateService = require("../services/validateService");
const { validate: uuidValidate } = require("uuid");
const validateService = new ValidateService();
const mediaService = require("../services/mediaService");

class UserController {
    static async createNewUser(req, res, next) {
        try {
            const { name, email, phone, password, roleId } = req.body;
            if (!name || !email || !phone || !password || !roleId) {
                return res
                    .status(400)
                    .json({ error: "Please provide name, email and password" });
            }
            const isValidEmail = await validateService.isValidateEmail(email);
            if (!isValidEmail) {
                return res.status(400).json({ error: "Email not format" });
            }
            const isValidPhone = await validateService.isValidatePhone(phone);
            if (!isValidPhone) {
                return res
                    .status(400)
                    .json({ error: "Phone Number not format" });
            }
            const isValidPassword = await validateService.isValidatePassword(
                password
            );
            if (!isValidPassword) {
                return res.status(400).json({ error: "Password not format" });
            }
            console.log(req.body);
            const existingUser = await UserService.getUserByEmail(email);
            if (existingUser) {
                return res
                    .status(400)
                    .json({ error: "User is already exists" });
            }
            const newUser = await UserService.createNewUser(
                name,
                email,
                phone,
                password,
                roleId
            );
            if (newUser) {
                return res
                    .status(201)
                    .json({ message: "Create new User successfully!" });
            }
            res.status(400).json({ error: "Create new User failed!" });
        } catch (err) {
            next(err);
        }
    }

    static async Login(req, res) {
        const { email, password } = req.body;
        const user = await UserService.findUserByEmail(email);
        if (!user) {
            return res.status(400).json({ error: "Can't find user" });
        }
        const isPassword = await UserService.isCheckUserPassword(
            password,
            user.password
        );
        if (!isPassword) {
            return res.status(400).json({ error: "Password incorrect!" });
        }
        delete user.dataValues.password;
        const token = JWTService.GenerateToken(user);
        return res.status(200).json({
            data: {
                accessToken: token.token,
                refreshToken: token.refreshToken,
            },
        });
    }

    static async getUser(req, res) {
        const id = req.params.id;
        const isValidateUUID = uuidValidate(id);
        if (!isValidateUUID) {
            return res.status(400).json({ error: "UUID User inValid!" });
        }
        const user = await UserService.findUserById(id);
        if (!user) {
            return res.status(400).json({ error: "Can't find user" });
        }
        delete user.dataValues.password;
        return res.status(200).json({
            status: 200,
            data: user,
        });
    }

    static async getCurrentUser(req, res) {
        const id = req.user.user.id;
        console.log(id);
        const isValidateUUID = uuidValidate(id);
        if (!isValidateUUID) {
            return res.status(400).json({ error: "UUID User inValid! 111" });
        }
        const user = await UserService.findUserById(id);
        if (!user) {
            return res.status(400).json({ error: `Can't find user by ${id}` });
        }
        delete user.dataValues.password;
        return res.status(200).json({
            status: 200,
            data: user,
        });
    }

    static async getAllUsers(req, res) {
        const users = await UserService.getAllUsers();
        const listUsers = await UserService.ParseToJSON(users);
        const userWithoutPassword = listUsers.map(
            ({ password, ...listUsers }) => listUsers
        );
        return res.status(200).json({
            status: 200,
            data: userWithoutPassword,
        });
    }

    static async refreshToken(req, res) {
        const refeshToken = req.body.refreshToken;
        const email = JWTService.VerifyRefreshToken(refeshToken);
        if (!email) {
            return res.status(400).json({ error: "Refresh token inValid!" });
        }
        const user = await UserService.findUserByEmail(email);
        if (!user) {
            return res.status(400).json({ error: "Can't find user" });
        }
        delete user.dataValues.password;
        const token = JWTService.GenerateToken(user);
        return res.status(200).json({
            status: 200,
            data: {
                accessToken: token.token,
                refreshToken: token.refreshToken,
            },
        });
    }

    static async uploadAvatar(req, res, next) {
        try {
            const id = req.user.user.id;
            await mediaService
                .uploadAvatar(req)
                .then((result) => {
                    const userUpadate = UserService.changeAvatar(result, id);
                    if (userUpadate) {
                        return res
                            .status(201)
                            .json({ message: "uploaded avatar!" });
                    }
                })
                .catch((error) => {
                    console.log(error.message);
                    return res
                        .status(400)
                        .json({ error: "upload avatar failed" });
                });
        } catch (err) {
            next(err);
        }
    }
}

module.exports = UserController;
