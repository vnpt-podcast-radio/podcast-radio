const JWTService = require("../src/services/jwtService");

class AuthMiddleware {
    static async Authenticate(req, res, next) {
        let token = req.headers.authorization;

        if (!token) {
            return res.status(401).json({ error: "Unauthorized" });
        }
        const decoded = JWTService.VerifyToken(token.split(" ")[1]);
        if (!decoded) {
            return res.status(401).json({ error: "Unauthorized" });
        }
        req.user = decoded;
        next();
    }
}

module.exports = AuthMiddleware;
