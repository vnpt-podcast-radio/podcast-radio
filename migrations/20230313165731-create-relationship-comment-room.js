"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addColumn("Comments", "roomId", {
            type: Sequelize.UUID,
            references: {
                model: "Rooms",
                key: "id",
            },
            onDelete: "CASCADE",
            onUpdate: "CASCADE",
        });
        await queryInterface.addColumn("Comments", "userId", {
            type: Sequelize.UUID,
            references: {
                model: "Users",
                key: "id",
            },
            onDelete: "CASCADE",
            onUpdate: "CASCADE",
        });
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeColumn("Comments", "roomId");
        await queryInterface.removeColumn("Comments", "userId");
    },
};
