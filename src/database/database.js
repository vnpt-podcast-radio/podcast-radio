const { Sequelize } = require("sequelize");

const config = require("../../config/config.js");

const sequelize = new Sequelize(
    config.development.database,
    config.development.username,
    config.development.password,{
        host: "localhost",
        port: config.development.port,
        dialect: config.development.dialect
    
});

module.exports = sequelize;