const { DataTypes } = require("sequelize");
const sequelize = require("../database/database");
const Singer = require("./Singer");
const Song = require("./Song");

const SingerSong = sequelize.define("SingerSong", {
    id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
    },
    SingerId: {
        allowNull: false,
        type: DataTypes.UUID,
        references: {
            key: "id",
            model: {
                tableName: "Singers",
            },
        },
        field: "singerId",
    },
    SongId: {
        allowNull: false,
        type: DataTypes.UUID,
        references: {
            key: "id",
            model: {
                tableName: "Songs",
            },
        },
        field: "songId",
    },
});

Singer.belongsToMany(Song, { through: SingerSong, as: "songs" });
Song.belongsToMany(Singer, { through: SingerSong, as: "singers" });

module.exports = SingerSong;
