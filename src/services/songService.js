const Singer = require("../models/Singer");
const Song = require("../models/Song");
const SingerSongs = require("../models/SingerSong");
const MediaService = require("./mediaService");

class SongService {
    //CREATE NEW SONG
    static async createNewSong(name, url_song, public_id, singers) {
        try {
            console.log(singers);
            const song = Song.create({ name, url_song, public_id });
            const songId = (await song).dataValues.id;
            if (song) {
                singers.map((value) => {
                    this.CreateSingerSong(value.id, songId);
                });
            }
            return song;
        } catch (err) {
            throw err;
        }
    }

    //CREATE RELATIONSHIP SINGER_SONG
    static async CreateSingerSong(singerId, songId) {
        try {
            const singersong = SingerSongs.create({
                SingerId: singerId,
                SongId: songId,
            });
            return singersong;
        } catch (err) {
            throw err;
        }
    }

    //DELETE
    static async deleteSong(id) {
        return Song.findByPk(id)
            .then((res) => {
                MediaService.deleteAudio(res.dataValues.public_id);
                return res.destroy();
            })
            .catch((err) => {
                return null;
            });
    }
}

module.exports = SongService;
