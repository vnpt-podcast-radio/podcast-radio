const { DataTypes } = require("sequelize");
const { Sequelize } = require("sequelize");
const sequelize = require("../database/database");
const User = require("./User");
const Room = require("./Room");

const RoomSongs = sequelize.define("RoomSongs", {
    id: {
        type: Sequelize.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
    },
    roomId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
            model: "Rooms",
            key: "id",
        },
    },
    songId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
            model: "Songs",
            key: "id",
        },
    },
    time_start: {
        type: Sequelize.DATE,
        allowNull: true,
    },
    time_end: {
        type: Sequelize.DATE,
        allowNull: true,
    },
});
Room.hasMany(RoomSongs, { foreignKey: "roomId", as: "roomSongs" });
RoomSongs.belongsTo(Room, { foreignKey: "roomId", as: "room" });
Song.hasMany(RoomSongs, { foreignKey: "songId", as: "songs" });
RoomSongs.belongsTo(Song, { foreignKey: "songId", as: "song" });

module.exports = RoomSongs;
