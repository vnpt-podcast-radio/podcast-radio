require("dotenv").config();
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const User = require("./src/models/User");
const UserService = require("./src/services/userService");
const CommentService = require("./src/services/commentService");
const RoomService = require("./src/services/roomService");

const io = require("socket.io")(3333, {
    cors: {
        origin: "*",
    },
});

const route = require("./src/routes/route");

app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);
app.use(bodyParser.json());
app.use(cors());
app.use(
    cors({
        origin: "*",
        methods: ["GET", "POST", "PUT", "DELETE"],
        allowedHeaders: ["Authorization", "Content-Type"],
    })
);
const port = process.env.port || process.env.API_PORT;

app.use(bodyParser.json());

app.use("/api", route);

io.on("connection", (socket) => {
    console.log("New client connected!" + socket.id);

    socket.on("join-room", (data) => {
        const { roomId, email } = data;
        const socketId = socket.id;
        socket.join(roomId);
        const user = UserService.getUserByEmail(email);
        user.then((res) => {
            const data = {
                name: res.dataValues.name,
                socketId: socketId,
            };
            console.log(data);
            io.to(roomId).emit("joined", data);
            console.log("Đã vào rồi đừng reload ạ!");
        });
        socket.on("song-info", (data) => {
            socket.to(roomId).to(socketId).emit("get-song-info", data);
        });
        socket.on("chat-message", (data) => {
            console.log(data);
            CommentService.SaveComment(data.userId, roomId, data.message);
            io.to(roomId).emit("message", {
                message: data.message,
                email: data.email,
            });
        });

        socket.on("stop-room", () => {
            RoomService.stopRoom(roomId);
            io.to(roomId).emit("room-stopped");
        });

        socket.on("send-status-song", (status) => {
            console.log(status);
            io.to(roomId).emit("status-song", status);
        });

        socket.on("change-song-in-room", (song) => {
            io.to(roomId).emit("event-song", {
                song: song,
            });
        });
    });

    socket.on("disconnect", () => {
        console.log("Client disconnected.");
    });
});

app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});

module.exports = io;
