'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable("Channel",{
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING
      },
      desribe: {
        allowNull: false,
        type: Sequelize.STRING
      },
      url_avatar: {
        allowNull: false,
        type: Sequelize.STRING
      },
      url_cover: {
        allowNull: false,
        type: Sequelize.STRING
      },
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable("Channel");
  }
};
