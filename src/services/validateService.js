//validator Email Format
class ValidateService {
    async isValidateEmail(email) {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        return emailRegex.test(email);
    }

    async isValidatePhone(phone) {
        const phoneRegex = /^(0|\+84)[3|5|7|8|9][0-9]{8}$/;
        return phoneRegex.test(phone);
    }

    async isValidatePassword(password) {
        const passwordRegex =
            /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*])(?=.{8,})/;
        return passwordRegex.test(password);
    }
}

module.exports = ValidateService;
