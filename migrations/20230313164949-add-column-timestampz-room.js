"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.addColumn("Comments", "createdAt", {
            type: Sequelize.DATE,
            allowNull: false,
        });
        await queryInterface.addColumn("Comments", "updatedAt", {
            type: Sequelize.DATE,
            allowNull: false,
        });
    },

    async down(queryInterface, Sequelize) {
        await queryInterface.removeColumn("Comments", "createdAt");
        await queryInterface.removeColumn("Comments", "updatedAt");
    },
};
