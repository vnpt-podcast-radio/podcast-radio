const io = require("../../server");
const RoomService = require("../services/roomService");
const uuid = require("uuid");
const Room = require("../models/Room");
const User = require("../models/User");
const UserService = require("../services/userService");
const { validate: uuidValidate } = require("uuid");
const Comment = require("../models/Comment");

class RoomController {
    static async createRoom(req, res) {
        const id = req.user.user.id;
        const isValidUUID = uuidValidate(id);
        if (!isValidUUID) {
            return res.status(400).json({ error: "UUID User inValid!" });
        }
        const { name, describe } = req.body;
        if (describe.length > 255) {
            return res
                .status(400)
                .json({ error: "Description is over 255 characters" });
        }
        if (!name || !describe) {
            return res.status(400).json({ error: "Name or Desribe be Empty!" });
        }
        const roomId = uuid.v4();
        const room = await RoomService.newRoom(roomId, name, describe, id);
        if (room) {
            return res.status(200).json({ roomId: roomId });
        }
        return res.status(400).json({ message: "Create Room Error" });
    }

    static async getRoomById(req, res) {
        const id = req.params.id;
        const isValidUUID = uuidValidate(id);
        if (!isValidUUID) {
            return res.status(400).json({ error: "UUID Room inValid!" });
        }
        const room = await Room.findOne({
            where: {
                id: id,
            },
            include: {
                model: User,
                as: "user",
            },
        });
        if (room) {
            return res.status(200).json({
                status: 200,
                data: room,
            });
        }
        return res.status(400).json({ message: "Room Not Found!" });
    }

    static async getRooms(req, res) {
        const rooms = await Room.findAll({
            include: {
                model: User,
                as: "user",
            },
            where: {
                status: true,
            },
        });
        if (rooms) {
            const roomDatas = await UserService.ParseToJSON(rooms);
            const listRooms = roomDatas.map(
                ({ user: { password, ...resUser }, ...roomData }) => ({
                    user: resUser,
                    ...roomData,
                })
            );
            return res.status(200).json({
                status: 200,
                data: listRooms,
            });
        }
        return res.status(400).json({ message: "Rooms Not Found!" });
    }

    static async deleteRoomById(req, res) {
        const id = req.params.id;
        const isValidUUID = uuidValidate(id);
        if (!isValidUUID) {
            return res.status(400).json({ error: "UUID Room inValid!" });
        }
        const userId = req.user.user.id;
        await RoomService.deleteRoom(id, userId)
            .then((room) => {
                if (room === null) {
                    return res
                        .status(400)
                        .json({ error: "No permission to delete!" });
                }
                return res.status(200).json({ error: "Room delete!" });
            })
            .catch((error) => {
                res.status(500).json({ error: "Internal server error" });
            });
    }
}

module.exports = RoomController;
