const { DataTypes } = require("sequelize");
const { Sequelize } = require("sequelize");
const sequelize = require("../database/database");
const User = require("./User");

const Role = sequelize.define("Role", {
  id: {
    allowNull: false,
    primaryKey: true,
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
  },
  name: {
    allowNull: false,
    type: DataTypes.STRING,
  },
});

module.exports = Role;
