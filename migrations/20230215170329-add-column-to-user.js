'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.addColumn("Users", "createAt", {
      type: Sequelize.DATE
    });

    await queryInterface.addColumn("Users", "upadateAt", {
      type: Sequelize.DATE
    });
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.removeColumn("Users", "createAt");
    await queryInterface.removeColumn("Users", "updateAt");
  }
};
