const { DataTypes } = require("sequelize");
const { Sequelize } = require("sequelize");
const sequelize = require("../database/database");
const Song = require("./Song");

const Singer = sequelize.define("Singer", {
    id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
    },
    name: {
        allowNull: false,
        type: DataTypes.STRING,
    },
});
Singer.associations = () => {
    Singer.belongsToMany(Song, {
        through: "SingerSongs",
        foreignKey: "songId",
    });
};

module.exports = Singer;
