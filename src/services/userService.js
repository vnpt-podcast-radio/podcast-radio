const User = require("../models/User");
const Room = require("../models/Room");
const PasswordService = require("./encryptionService");
const mediaService = require("./mediaService");
const bcrypt = require("bcryptjs");
const passwordService = new PasswordService();
const uuid = require("uuid");
const Role = require("../models/Role");

class UserService {
    static async getUserByEmail(email) {
        const user = await User.findOne({
            where: {
                email: email,
            },
        });
        return user;
    }

    static async createNewUser(name, email, phone, pass, roleId) {
        const id = uuid.v4();
        try {
            const password = await passwordService.hashPassword(pass);
            const user = User.create({
                id,
                name,
                email,
                phone,
                password,
                roleId,
            });
            console.log("Successfully!");
            return user;
        } catch (err) {
            throw err;
        }
    }

    static async getAllUsers() {
        const users = await User.findAll({
            include: {
                model: Room,
                as: "rooms",
            },
        });
        return users;
    }

    static async findUserById(id) {
        const user = await User.findOne({
            where: {
                id: id,
            },
            include: [
                {
                    model: Room,
                    as: "rooms",
                },
                {
                    model: Role,
                    as: "role",
                },
            ],
        });
        return user;
    }

    static async findUserByEmail(email) {
        const user = User.findOne({
            where: {
                email: email,
            },
        });
        return user;
    }

    static async isCheckUserPassword(password, userPassword) {
        const isMatch = await bcrypt.compare(password, userPassword);
        return isMatch;
    }

    static async ParseToJSON(data) {
        const json = JSON.stringify(data);
        return JSON.parse(json);
    }

    static async changeAvatar(result, userId) {
        try {
            const avatar = {
                avatar: result.url,
                avatar_public_id: result.public_id,
            };
            const user = User.findOne({ where: { id: userId } })
                .then((result) => {
                    if (result.dataValues.avatar_public_id) {
                        mediaService.deleteAudio(
                            result.dataValues.avatar_public_id
                        );
                        return result.update(avatar);
                    }
                    return result.update(avatar);
                })
                .catch((err) => {
                    return err;
                });
            return user;
        } catch (err) {
            throw err;
        }
    }
}

module.exports = UserService;
